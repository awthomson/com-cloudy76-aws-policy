# AWS Policy Generator

This is a simple AWS policy creater.  Most usefully is it acts as a quick way to search across all AWS actions.

A link to the live site is here: https://aws-policy.cloudy76.com

Other tools you might find useful:
* YAJE - Yet Another JSON Editor (and validator!) https://yaje.cloudy76.com
* SimpleSecret - A secure way to send secrets over the internet https://simplesecret.cloudy76.com
