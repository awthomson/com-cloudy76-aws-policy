import React from 'react';
import Actions from './Actions.js';

class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
			action: "",
			resource: "",
			policy: "{}",
			filtered: [ ],
			arnformat: ""
        };
		this.updateInputAction = this.updateInputAction.bind(this);
		this.updateInputResource = this.updateInputResource.bind(this);
    }

	updateInputAction = (event) => {
		var action = event.target.value
		this.setState({action: action})
		var filtered = []
	
		// Don't bother if it's only a couple of characters
		if (action.length < 2)
			return;

		const act = action.split(':');
		action = act[0];
		const subaction = act[1];

		if (subaction == null) {
			// Search for matching actions
			for (var i = 0; i < Actions.length; i++) {
				if (Actions[i].StringPrefix.includes(action)) {
    				console.log(Actions[i]);
					filtered.push({text: Actions[i].StringPrefix+":*"});
				}
			}
		} else {
			// An action is specified, so search for matching subactions
			for (var j = 0; j < Actions.length; j++) {
				if (Actions[j].StringPrefix.localeCompare(action) === 0) {
					var a = Actions[j].Actions;
					console.log("Found action "+action);
					this.setState({arnformat: Actions[j].ARNFormat});
					for (var k=0; k<a.length; k++) {
						if (a[k].includes(subaction)) {
							var text = Actions[j].StringPrefix+":"+a[k];
							filtered.push({text: text});
						}
					}
				}
			}
		}
		this.setState({filtered: filtered});
	}

	updateInputResource = (event) => {
		this.setState({resource: event.target.value})
	}

	createPolicy = () => {
		var frag = `
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "${this.state.action}"
      ],
      "Resource": [
        "${this.state.resource}"
      ]
    }
  ]
}
`
		this.setState({policy: frag})
	}

	render() {
			console.log(Actions);
			return (
				<div className="form">
					<div className="formTitle">AWS Policy Creator</div>
					<h1>IAM Policy Creator Wizard</h1>

					<hr/>
					<p>Use this little tool to help create simple AWS policies.  This uses a full set of AWS actions valid as of December 2021.</p>

					<label>Action:</label>
					<input onChange={this.updateInputAction} value={this.state.action}></input>
					<br/>

					<ul>
   					{
      					this.state.filtered.map(f => {
         					return <li>{f.text}</li>
      					})
   					}
					</ul>

					<label>Resource:</label>
					<input onChange={this.updateInputResource} value={this.state.resource}></input>
					<ul><li>{this.state.arnformat}</li></ul>
					<br/>

					<div className="buttonBar">
					<button onClick={this.createPolicy} className="action">Update</button>
					</div>

					<hr/>

					<div class="output">
					<pre>
						{this.state.policy}
					</pre>
					</div>

				</div>
			);
		} 

}

export default Home;
