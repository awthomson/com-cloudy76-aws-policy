#!/bin/bash

rm services.json

echo "Downloading latest policy set..."
curl -s -o actions.json https://awspolicygen.s3.amazonaws.com/js/policies.js

echo "Munging data..."
sed -i 's/app.PolicyEditorConfig=//' actions.json

x=$(jq .serviceMap actions.json | jq -r keys[] -)
while IFS= read -r line; do
    echo -n "  * $line ..."

	# Put each stanza in it's own temporary file
	jq ".serviceMap.\"$line\"" actions.json > tmp.txt
	
	# Add the "Name" key near the top of the JSON blob
	awk "NR==2{print \"  'Name':'$line',\"}1" tmp.txt > tmp2.txt

	echo "," >> tmp2.txt
	sed -i "s/'/\"/g" tmp2.txt

	cat tmp2.txt >> services.json
	echo "Adding"

done <<< "$x"

# Remove the last comma in the aggregated file
sed -i '$ d' services.json

echo "var Actions = [" > Actions.js
cat services.json >> Actions.js
echo "];" >> Actions.js
echo "export default Actions" >> Actions.js

rm tmp.txt
rm tmp2.txt
rm actions.json
rm services.json

