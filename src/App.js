import './App.css';
import Home from './Home';
import Footer from './Footer';

function App() {
  return (
    <div className="App">
	  <Home/>
	  <Footer/>
    </div>
  );
}
      // <header className="App-header"></header>

export default App;
