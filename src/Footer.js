import React from 'react';

class Footer extends React.Component {

	componentDidMount() {
		document.title = "AWS Policy Creator"
	}

	render() {
		return (
			<div className="footer">
				<div><a href="https://gitlab.com/awthomson/com-cloudy76-iaws-policy">AWS Policy Creator Source Code</a></div>
				<div>Copyright © 2022 - cloudy76.com</div>
				<div>
					<ul>
						<li><a href="https://yaje.cloudy76.com">YAJE - Yet Another JSON Editor</a></li>
						<li><a href="https://simplesecret.cloudy76.com">SimpleSecret - Secret Sharing Tool</a></li>
					</ul>
				</div>
			</div>
		);
	}

}
					// <a href={process.env.REACT_APP_URL}>Home</a> 

export default Footer;
